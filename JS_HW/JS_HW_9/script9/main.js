const tabs=document.querySelector(`.tabs`);
//console.log(tabs)

const tabsText = document.querySelectorAll('.tabs-content li');

tabs.addEventListener(`click`, (event) => {
    event.target.closest('ul').querySelector('.active').classList.remove('active')
    event.target.classList.add('active');

    const tabData = event.target.dataset.tab

    tabsText.forEach(item => {
        item.setAttribute('hidden', 'true');
        if (item.dataset.text === tabData) {
            item.removeAttribute('hidden')
        }

    })



    console.log(tabData)
})