function createNewUser() {

    this.userName = prompt('Enter your first name: ');
    this.userLastName = prompt('Enter your last name');
    this.birthDate = prompt(`Enter your full birthdate`, `dd.mm.yyyy`);


    this.getLogin = function () {
        return this.userName.charAt(0).toLowerCase() + this.userLastName.toLowerCase();
    }
    this.getPassword = function () {
        return this.userName[0].toUpperCase() + this.userLastName + this.birthDate.slice(-4)
    }

    this.getAge = function () {
        const currentDate = new Date() //return current date with hours, seconds
        const currentYear = currentDate.getFullYear()   //2021
        const userBirthYear = this.birthDate.slice(-4)
        return currentYear - userBirthYear

    }

}

const newUser = new createNewUser();

console.log(newUser.getLogin());
console.log(newUser.getPassword());
console.log(newUser.getAge());

